FTM is a shared library for Max/MSP providing a small and simple real-time object system and a set of optimized services to be used within Max/MSP externals.

**FTM  = data structures**  
**+ visualization/editors**  
**+ file import/export (SDIF, MIDI, ...)**  
**+ operators (expressions and externals)**  

The basic idea of FTM is to extend the data types exchanged between the objects in a Max/MSP patch by complex data structures such as sequences, matrices, dictionaries, break point functions, tuples and whatever might seem helpful for the processing of music, sound and motion capture data.

The FTM library and its Java editors are based on the jMax project and distributed under the Lesser GNU Public License (LGPL). The sources of FTM are available via CVS at SourceForge.net

### Data structures

FTM allows for static and dynamic creation of complex data structures. The following classes are currently implemented and documented:

- **mat**	...	matrix of arbitrary values
- **dict**	...	dictionary of arbitrary key/value pairs
- **sequence**	...	sequence of time-tagged values
- **fmat**	...	matrix of floats
- **bpf**	...	break point function
- **tuple**	...	immutable array of arbitrary values
- **scoob**	...	score object (note, trill, rest, etc.)
- **midi**	...	midi event

FTM objects can contain references to other FTM objects. A simple garbage collector handles transparently the destruction of dynamically created FTM objects referenced by multiple elements of a patch.

Static FTM objects are created in a patcher using a dedicated Max/MSP external called ftm.object.

![ftm.object.gif](https://git.forum.ircam.fr/borghesi/ftm/-/raw/master/readme-images/ftm.object.gif)

*Example of a static FTM object in a Max/MSP patcher*


They can be named within a local or global scope and marked persistent to be saved within the patcher. FTM provides a serialization mechanism to recursively save the content of objects and the contained objects.

FTM externals can refer to FTM objects and dynamically create objects. FTM objects can be sent in lists or as single values using a dedicated FTM object message.

While basic operations of FTM objects are implemented as methods of the FTM classes, more complex interactions with FTM objects use operators in form of Max/MSP externals.

### Operators

FTM comes with an extended message box - the ftm.mess external - which integrates classical Max message syntax with FTM names, infix expression evaluation, function calls and method invocation including return values.

![ftm.mess.gif](https://git.forum.ircam.fr/borghesi/ftm/-/raw/master/readme-images/ftm.mess.gif)

 *Examples of the FTM message box*


Apart from ftm.object and ftm.mess, FTM includes a set of externals implementing basic functionalities such as iteration, playing, interpolation, etc. and the conversion between FTM objects and Max values or lists.

![ftm.operators.gif](https://git.forum.ircam.fr/borghesi/ftm/-/raw/master/readme-images/ftm.operators.gif)

*Example of FTM externals operating on FTM objects (the MIDI parsing objects convert a MIDI byte stream into a stream of FTM MIDI events)*


Operators specific to a certain field of applications are assembled to separate object sets such as Gabor for sound analysis/resynthesis and MnM for mapping and recognition techniques.

### Editors and file formats

The editors for FTM objects are implemented in Juce and integrated into Max/MSP using mxj. Simple double-clicking on an FTM object in a patcher opens its editor.

![ftm.trackeditor.gif](https://git.forum.ircam.fr/borghesi/ftm/-/raw/master/readme-images/ftm.trackeditor.gif)

 *Screen shot of the track editor for score objects*


FTM currently supports simple text files, SDIF and standard MIDI files.

Most of these import and export methods are implemented for the FTM sequence class as it provides a generic container for temporal sequences.

### Jitter & JavaScript support

From a certain point of view one can look at FTM as something like Jitter. Like Jitter, it is a shared library installed next to the Max/MSP application. Like Jitter, it extends the set of things one can send through the connections and process by Max/MSP externals.

A bridge between FTM and Jitter - the ftm.jitter external - converts FTM matrices into Jitter matrices and vice versa.

FTM objects can also be created, accessed and manipulated in the Max/MSP JavaScript externals js and jsui.
